class Main {
	constructor() {
        this.$pageArticle = document.querySelector('.page-1');
        this.$pageVideos = document.querySelector('.page-2');
        this.$butVideos = document.querySelector('.dropdown');
        this.$butArticle = document.querySelector('.article');
        this.$topNavMenu = document.querySelector('.nav');
        this.$bottomNavMenu = document.querySelector('.nav-bottom');
        this.$topNavMenu.addEventListener('click',this.showPage.bind(this));
        this.$bottomNavMenu.addEventListener('click',this.showPage.bind(this));
    }
    showPage(event) {
        const bottomVideoBtn = event.target;
        const text = event.target.innerText.toLowerCase();
        if ( text === 'most recent' || text === 'most popular' || text === 'all' || bottomVideoBtn.classList.value === 'bottom-video-btn') {
            event.preventDefault();
            this.$butArticle.classList.remove('border-co'); 
            this.$butVideos.classList.add('border-co');
            this.$pageArticle.classList.add('hide');
            this.$pageVideos.classList.remove('hide');

        } else if ( text === 'articles') {
            event.preventDefault();
            this.$butVideos.classList.remove('border-co');
            this.$butArticle.classList.add('border-co'); 
            this.$pageArticle.classList.remove('hide');
            this.$pageVideos.classList.add('hide');
        }
	}
}
document.addEventListener('DOMContentLoaded',()=> {
	this.main = new Main();
});


